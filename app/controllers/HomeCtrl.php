<?php

namespace template\app\controllers;

use Velocity\config\Config;
use Velocity\core\Controller;
use Velocity\helpers\Helpers;

class HomeCtrl extends Controller {

	public  $variable;

	public function init() {
		$this->variable = date('Y');	
	}

}

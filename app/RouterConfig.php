<?php

namespace Template\App;

use Velocity\Core\Router;

class RouterConfig {
	public static function execute() {
		/**
		 * These two routes point to the same resource, i.e., controller when accessed
		 * using a GET request
		 */
		Router::get('/', 'home');
		Router::get('/inicio', 'home');
		Router::get('/historia', 'historia');
		Router::get('/el-local', 'local');
		Router::get('/catalogo', 'catalogo');
		Router::get('/tu-creacion', 'creacion');
		




		/**
		 * This is a route with basic parameters. It uses PHP regular expressions with named
		 * capture groups. The capture group name is the name of the parameter sent to the
		 * by_id method in the time controller
		 */
		Router::get('/home/(?<id>[0-9]+)', 'home#by_id');

		/**
		 * When using multiple parameters, they are sent in order of appearance to
		 * the corresponding controller method
		 *
		 * Parameters can be defined to be of a certain type, like text or numbers only
		 */
		Router::get('/home/(?<name>[A-z]+)/hour/(?<hour>[0-9]+)/minute/(?<minute>[0-9]+)', 'home#multiple_params');
	}
}

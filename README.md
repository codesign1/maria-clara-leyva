## Velocity examples
Each one of the folders inside this repo contains the code of a self-contained project that uses the Velocity framework.

### Running an example
Firstly, `cd` into the folder. Secondly, `composer install`, and then run MAMP inside the corresponding folder.